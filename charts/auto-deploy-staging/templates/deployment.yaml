apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ template "trackableappname" . }}
  labels:
    app: {{ template "appname" . }}
    track: "{{ .Values.application.track }}"
    tier: "{{ .Values.application.tier }}"
    chart: "{{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}"
    release: {{ .Release.Name }}
    heritage: {{ .Release.Service }}
  annotations:
    fluentbit.io/parser: koa
spec:
  replicas: {{ .Values.replicaCount }}
  selector:
    matchLabels:
      app: {{ template "appname" . }}
  template:
    metadata:
      labels:
        app: {{ template "appname" . }}
        track: "{{ .Values.application.track }}"
        tier: "{{ .Values.application.tier }}"
        release: {{ .Release.Name }}
    spec:
      imagePullSecrets:
{{ toYaml .Values.image.secrets | indent 10 }}
      securityContext:
        runAsUser: 1000
      containers:
      - name: {{ .Chart.Name }}
        image: "{{ .Values.image.repository }}:{{ .Values.image.tag }}"
        imagePullPolicy: {{ .Values.image.pullPolicy }}
        env:
{{- if .Values.application.NODE_ENV }}
        - name: NODE_ENV
          value: {{ .Values.application.NODE_ENV | quote }}
{{end}}
{{- if .Values.application.SENTRY_DSN }}
        - name: SENTRY_DSN
          value: {{ .Values.application.SENTRY_DSN | quote }}
{{end}}
{{- if .Values.application.BASE_URL }}
        - name: BASE_URL
          value: {{ .Values.application.BASE_URL | quote }}
{{end}}
{{- if .Values.application.HOST }}
        - name: HOST
          value: {{ .Values.application.HOST | quote }}
{{end}}
{{- if .Values.application.DB_NAME }}
        - name: DB_NAME
          value: {{ .Values.application.DB_NAME | quote }}
{{end}}
{{- if .Values.application.APP_NAME }}
        - name: APP_NAME
          value: {{ .Values.application.APP_NAME | quote }}
{{end}}
{{- if .Values.application.MONGO_URI }}
        - name: MONGO_URI
          value: {{ .Values.application.MONGO_URI | quote }}
{{end}}
{{- if .Values.application.MONGO_REPLSET }}
        - name: MONGO_REPLSET
          value: {{ .Values.application.MONGO_REPLSET | quote }}
{{end}}
{{- if .Values.application.MONGO_USERNAME }}
        - name: MONGO_USERNAME
          value: {{ .Values.application.MONGO_USERNAME | quote }}
{{end}}
{{- if .Values.application.MONGO_PASSWORD }}
        - name: MONGO_PASSWORD
          value: {{ .Values.application.MONGO_PASSWORD | quote }}
{{end}}
{{- if .Values.application.MONGO_SSL }}
        - name: MONGO_SSL
          value: {{ .Values.application.MONGO_SSL | quote }}
{{end}}
{{- if .Values.application.REDIS_CLUSTER_HOSTS }}
        - name: REDIS_CLUSTER_HOSTS
          value: {{ .Values.application.REDIS_CLUSTER_HOSTS | quote }}
{{end}}
{{- if .Values.application.REDIS_SENTINEL_HOSTS }}
        - name: REDIS_SENTINEL_HOSTS
          value: {{ .Values.application.REDIS_SENTINEL_HOSTS | quote }}
{{end}}
{{- if .Values.application.REDIS_SENTINEL_PASSWORD }}
        - name: REDIS_SENTINEL_PASSWORD
          value: {{ .Values.application.REDIS_SENTINEL_PASSWORD | quote }}
{{end}}
{{- if .Values.application.REDIS_PASSWORD }}
        - name: REDIS_PASSWORD
          value: {{ .Values.application.REDIS_PASSWORD | quote }}
{{end}}
{{- if .Values.application.REDIS_TLS }}
        - name: REDIS_TLS
          value: {{ .Values.application.REDIS_TLS | quote }}
{{end}}
{{- if .Values.application.RP_NAME }}
        - name: RP_NAME
          value: {{ .Values.application.RP_NAME | quote }}
{{end}}
{{- if .Values.application.RP_ID }}
        - name: RP_ID
          value: {{ .Values.application.RP_ID | quote }}
{{end}}
{{- if .Values.application.SMTP_HOST }}
        - name: SMTP_HOST
          value: {{ .Values.application.SMTP_HOST | quote }}
{{end}}
{{- if .Values.application.SMTP_PORT }}
        - name: SMTP_PORT
          value: {{ .Values.application.SMTP_PORT | quote }}
{{end}}
{{- if .Values.application.SMTP_USERNAME }}
        - name: SMTP_USERNAME
          value: {{ .Values.application.SMTP_USERNAME | quote }}
{{end}}
{{- if .Values.application.SMTP_PASSWORD }}
        - name: SMTP_PASSWORD
          value: {{ .Values.application.SMTP_PASSWORD | quote }}
{{end}}
{{- if .Values.application.MAIL_FROM }}
        - name: MAIL_FROM
          value: {{ .Values.application.MAIL_FROM | quote }}
{{end}}
{{- if .Values.application.UNOCONV_API_URL }}
        - name: UNOCONV_API_URL
          value: {{ .Values.application.UNOCONV_API_URL | quote }}
{{end}}
{{- if .Values.application.MINIO_ENDPOINT }}
        - name: MINIO_ENDPOINT
          value: {{ .Values.application.MINIO_ENDPOINT | quote }}
{{end}}
{{- if .Values.application.MINIO_DEFAULT_BUCKET }}
        - name: MINIO_DEFAULT_BUCKET
          value: {{ .Values.application.MINIO_DEFAULT_BUCKET | quote }}
{{end}}
{{- if .Values.application.ZAMMAD_API_BASE_URL }}
        - name: ZAMMAD_API_BASE_URL
          value: {{ .Values.application.ZAMMAD_API_BASE_URL | quote }}
{{end}}
{{- if .Values.application.ZAMMAD_GROUP }}
        - name: ZAMMAD_GROUP
          value: {{ .Values.application.ZAMMAD_GROUP | quote }}
{{end}}
{{- if .Values.application.ZIP_ENDPOINT }}
        - name: ZIP_ENDPOINT
          value: {{ .Values.application.ZIP_ENDPOINT | quote }}
{{end}}
{{- if .Values.application.ZIP_PORT }}
        - name: ZIP_PORT
          value: {{ .Values.application.ZIP_PORT | quote }}
{{end}}
{{- if .Values.application.cookieSecretName }}
        - name: COOKIE_SECRET
          valueFrom:
            secretKeyRef:
              name: {{ .Values.application.cookieSecretName | quote }}
              key: secret
{{end}}
{{- if .Values.application.jwtSecretName }}
        - name: JWT_KEY
          valueFrom:
            secretKeyRef:
              name: {{ .Values.application.jwtSecretName | quote }}
              key: jwt.hs256
{{end}}
{{- if .Values.application.mongoAuthSecretName }}
        - name: MONGO_USERNAME
          valueFrom:
            secretKeyRef:
              name: {{ .Values.application.mongoAuthSecretName | quote }}
              key: username
        - name: MONGO_PASSWORD
          valueFrom:
            secretKeyRef:
              name: {{ .Values.application.mongoAuthSecretName | quote }}
              key: password
{{end}}
{{- if .Values.application.minioSecretName }}
        - name: MINIO_ACCESS_KEY
          valueFrom:
            secretKeyRef:
              name: {{ .Values.application.minioSecretName | quote }}
              key: key
        - name: MINIO_SECRET_KEY
          valueFrom:
            secretKeyRef:
              name: {{ .Values.application.minioSecretName | quote }}
              key: secret
{{end}}
{{- if .Values.application.smtpSecretName }}
        - name: SMTP_USERNAME
          valueFrom:
            secretKeyRef:
              name: {{ .Values.application.smtpSecretName | quote }}
              key: username
        - name: SMTP_PASSWORD
          valueFrom:
            secretKeyRef:
              name: {{ .Values.application.smtpSecretName | quote }}
              key: password
{{end}}
{{- if .Values.application.redisAuthSecretName }}
        - name: REDIS_PASSWORD
          valueFrom:
            secretKeyRef:
              name: {{ .Values.application.redisAuthSecretName | quote }}
              key: password
        - name: REDIS_SENTINEL_PASSWORD
          valueFrom:
            secretKeyRef:
              name: {{ .Values.application.redisAuthSecretName | quote }}
              key: sentinel-password
{{end}}
{{- if .Values.application.cryptoSecretName }}
        - name: AES_KEY
          valueFrom:
            secretKeyRef:
              name: {{ .Values.application.cryptoSecretName | quote }}
              key: aes
        - name: HMAC_KEY
          valueFrom:
            secretKeyRef:
              name: {{ .Values.application.cryptoSecretName | quote }}
              key: hmac
{{end}}
{{- if .Values.application.postmarkSecretName }}
        - name: POSTMARK_API_TOKEN
          valueFrom:
            secretKeyRef:
              name: {{ .Values.application.postmarkSecretName | quote }}
              key: token
{{end}}
{{- if .Values.application.vplanEncryptionSecretName }}
        - name: VPLAN_ENCRYPTION_KEY
          valueFrom:+9
            secretKeyRef:
              name: {{ .Values.application.vplanEncryptionSecretName | quote }}
              key: key
{{end}}
{{- if .Values.application.zammadAPISecretName }}
        - name: ZAMMAD_API_TOKEN
          valueFrom:
            secretKeyRef:
              name: {{ .Values.application.zammadAPISecretName | quote }}
              key: token
{{end}}
        livenessProbe:
{{ toYaml .Values.livenessProbe | indent 12 }}
        readinessProbe:
{{ toYaml .Values.readinessProbe | indent 12 }}
        volumeMounts:
{{- if .Values.application.redisTLSSecretName }}
        - mountPath: /tls/redis
          name: {{ .Values.application.redisTLSSecretName | quote }}
{{end}}
{{- if .Values.application.mongoTLSSecretName }}
        - mountPath: /tls/mongo
          name: {{ .Values.application.mongoTLSSecretName | quote }}
{{end}}
        resources:
{{ toYaml .Values.resources | indent 12 }}
      volumes:
{{- if .Values.application.cookieSecretName }}
      - name: {{ .Values.application.cookieSecretName | quote }}
        secret:
          defaultMode: 256
          optional: false
          secretName: {{ .Values.application.cookieSecretName | quote }}
{{end}}
{{- if .Values.application.jwtSecretName }}
      - name: {{ .Values.application.jwtSecretName | quote }}
        secret:
          defaultMode: 256
          optional: false
          secretName: {{ .Values.application.jwtSecretName | quote }}
{{end}}
{{- if .Values.application.minioSecretName }}
      - name: {{ .Values.application.minioSecretName | quote }}
        secret:
          optional: false
          secretName: {{ .Values.application.minioSecretName | quote }}
{{end}}
{{- if .Values.application.cryptoSecretName }}
      - name: {{ .Values.application.cryptoSecretName | quote }}
        secret:
          optional: false
          secretName: {{ .Values.application.cryptoSecretName | quote }}
{{end}}
{{- if .Values.application.redisTLSSecretName }}
      - name: {{ .Values.application.redisTLSSecretName | quote }}
        secret:
          optional: false
          secretName: {{ .Values.application.redisTLSSecretName | quote }}
{{end}}
{{- if .Values.application.redisAuthSecretName }}
      - name: {{ .Values.application.redisAuthSecretName | quote }}
        secret:
          optional: false
          secretName: {{ .Values.application.redisAuthSecretName | quote }}
{{end}}
{{- if .Values.application.smtpSecretName }}
      - name: {{ .Values.application.smtpSecretName | quote }}
        secret:
          optional: false
          secretName: {{ .Values.application.smtpSecretName | quote }}
{{end}}
{{- if .Values.application.mongoTLSSecretName }}
      - name: {{ .Values.application.mongoTLSSecretName | quote }}
        secret:
          optional: false
          secretName: {{ .Values.application.mongoTLSSecretName | quote }}
{{end}}
{{- if .Values.application.mongoAuthSecretName }}
      - name: {{ .Values.application.mongoAuthSecretName | quote }}
        secret:
          optional: false
          secretName: {{ .Values.application.mongoAuthSecretName | quote }}
{{end}}
{{- if .Values.application.zammadAPISecretName }}
      - name: {{ .Values.application.zammadAPISecretName | quote }}
        secret:
          optional: false
          secretName: {{ .Values.application.zammadAPISecretName | quote }}
{{end}}
{{- if .Values.application.postmarkSecretName }}
      - name: {{ .Values.application.postmarkSecretName | quote }}
        secret:
          optional: false
          secretName: {{ .Values.application.postmarkSecretName | quote }}
{{end}}
{{- if .Values.application.vplanEncryptionSecretName }}
      - name: {{ .Values.application.vplanEncryptionSecretName | quote }}
        secret:
          optional: false
          secretName: {{ .Values.application.vplanEncryptionSecretName | quote }}
{{end}}

